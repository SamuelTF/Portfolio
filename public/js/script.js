// Using typed.js for easy typing animation

var options = {
  strings: ["<i>First</i> sentence.", "&amp; a second sentence."],
  typeSpeed: 40
}

var typed = new Typed(".content__body__container__text", options);
