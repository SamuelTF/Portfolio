// Dependencies
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const session = require('express-session');
const mongo = require('mongodb');
const mongoose = require('mongoose');

// Init app
const app = express();

// Connect database (no database needed for now)
// mongoose.connect();
// const db = mongoose.connection;

// Require / use routes
const routes = require('./routes/index');

app.use('/', routes);

// View engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Set local server
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function(){
  console.log('Portfolio server started on port ' + app.get('port'));
});
